function [stds,means] = ActualFrontier(N,M,weights,Cov,rbar)
%ACTUALFRONTIER 
% Inputs:
% N - number of portofolios
% M - asset numbers
% weights -2c weights
% Cov - cov matrix
% rbar - mean values

warning('off','all');

for i=1:N
    sum = 0;
    for j=1:M
        sum = sum + rbar(j)*weights(i,j);
    end
    means(i) = sum;
end

for s=1:N
    sum=0;
    for i=1:M
        for j=1:M
            varp = weights(s,i)*Cov(i,j)*weights(s,j);
            sum = sum+varp;
        end
    end
    stds(s)=sqrt(sum);
end

end

