function [w,r] = MaxExpectedReturn(rvec)
%MaxExpectedReturn 
% find the maximum expected return of a portofolio with no short selling
% constraint
% Inputs: 
% n - number of assets
% rvec - vector of asset returns for n assets
% Outputs:
% r - expected return of maximum expected return portofolio
% w - optimal weights
options = optimset('Display', 'off');
   w = linprog(-1 * rvec, -1 * eye(length(rvec)), ...
           zeros(length(rvec), 1), ones(1, length(rvec)), 1, [], [], ...
           [], options);
r = rvec*w;
end

