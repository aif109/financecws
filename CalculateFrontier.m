function [sigma_vec,rvec,weights] = CalculateFrontier(M, Cov, rbar, N)
%CALCULATEFRONTIER 
% Inputs:
% M - number of assets
% Cov - covariance matrix
% rbar - mean values
% N - number of portofolios


%determine the portofolio with lowest variance (no constraint on target
%return)
warning('off','all');

[wmin, varp_min, rmin]=MinVar(M,Cov,rbar);

%determine the portofolio with the highest expected return (no constraint
%on target return)
[wmax, rmax] = MaxExpectedReturn(rbar);

weights = zeros(N,M);

%determine the efficient frontier given a range of target returns
for i=1:N
    %efficient frontier
    target = rmin+i*(rmax-rmin)/10;
    [w, varp] = MP(M,Cov,rbar,target);
    weights(i,:) = w';
    rvec(i)=rbar*w;
    sigma_vec(i)=sqrt(varp);
end

end

