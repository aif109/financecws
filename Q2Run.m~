function Q2Run(time,time2,time3, portofolioNumber)
%Q2RUN 
%clc; clear; format short;

warning('off','all');

% 2 a,b

rbar = [0.006 0.01 0.014 0.018 0.022];
sigma = [0.085 0.08 0.095 0.09 0.1];
M = length(sigma);

corr = [1 0.3 0.3 0.3 0.3;...
        0.3 1 0.3 0.3 0.3;...
        0.3 0.3 1 0.3 0.3;...
        0.3 0.3 0.3 1 0.3;...
        0.3 0.3 0.3 0.3 1];
    
for i=1:M
    for j=1:M
        Cov(i,j)=sigma(i)*corr(i,j)*sigma(j);
    end
end

N=portofolioNumber;
[sigma_vec,rvec,rmin,rmax,weights] = CalculateFrontier(M, Cov, rbar, N);

h=figure(1)
clf

plot(sigma_vec, rvec, 'b');
legend('True Frontier');
legend('Location', 'Best');
xlabel('\sigma');
ylabel('r: Expected Portofolio Rate of Return');
title('True Frontier');

drawnow
saveas(h,'Q2b','png');

% 2 c

[sigmas,rvecs, weights] = EstimatedFrontier(time, rbar, Cov, N, M);

h=figure(2)
clf

plot(sigmas, rvecs, 'b');
legend('Estimated Frontier');
legend('Location', 'Best');
xlabel('\sigma');
ylabel('r: Estimated Portofolio Rate of Return');
title('Estimated Frontier');

drawnow
saveas(h,'Q2c','png');

% 2 d
% 2c portofolio weights, Cov, rbar (mean values initially), sigma
% to compute mean returns and std of this weights
[stds, means] = ActualFrontier(N,M,weights,Cov,rbar);

h=figure(3)
clf

plot(stds, means, 'b');
legend('Actual Frontier');
legend('Location', 'Best');
xlabel('\sigma');
ylabel('r: Actual Portofolio Rate of Return');
title('Actual Frontier');

drawnow
saveas(h,'Q2d','png');

h=figure(4)
clf

plot(sigma_vec, rvec, 'b');
hold all;
plot(sigmas, rvecs, 'r');
hold all;
plot(stds, means, 'g');
xlabel('\sigma');
ylabel('r: Portofolio Rate of Return');
title('One Month True, Estimated and Actual Frontier');

hleg = legend('True', 'Estimated', 'Actual');
set(hleg, 'FontAngle', 'italic', 'TextColor', [.3,.2,.1]);

drawnow
saveas(h,'Q2bcd','png');


% 2e
[avgSE,avgSA,avgRE,avgRA] = ExperimentalRun(time,rbar,Cov,N,M);


h=figure(5)
clf

plot(sigma_vec, rvec, 'b');
hold all;
plot(avgSE, avgRE, 'r');
hold all;
plot(avgSA, avgRA, 'g');
xlabel('\sigma');
ylabel('r: Portofolio Rate of Return');
title('24 Months Average Portofolio');

hleg = legend('True Frontier', 'Estimated Average Frontier', 'Actual Average Frontier');
set(hleg, 'FontAngle', 'italic', 'TextColor', [.3,.2,.1]);

drawnow
saveas(h,'Q2e','png');


% 2f
[avgSE,avgSA,avgRE,avgRA] = ExperimentalRun(time2,rbar,Cov,N,M);

h=figure(6)
clf

plot(sigma_vec, rvec, 'b');
hold all;
plot(avgSE, avgRE, 'r');
hold all;
plot(avgSA, avgRA, 'g');
xlabel('\sigma');
ylabel('r: Portofolio Rate of Return');
title('360 Months Average Portofolio');

hleg = legend('True', 'Estimated Average', 'Actual Average');
set(hleg, 'FontAngle', 'italic', 'TextColor', [.3,.2,.1]);

drawnow
saveas(h,'Q2f360','png');

[avgSE,avgSA,avgRE,avgRA] = ExperimentalRun(time3,rbar,Cov,N,M);

h=figure(7)
clf

plot(sigma_vec, rvec, 'b');
hold all;
plot(avgSE, avgRE, 'r');
hold all;
plot(avgSA, avgRA, 'g');
xlabel('\sigma');
ylabel('r: Portofolio Rate of Return');
title('1800 Months Average Portofolio');

hleg = legend('True', 'Estimated Average', 'Actual Average');
set(hleg, 'FontAngle', 'italic', 'TextColor', [.3,.2,.1]);

drawnow
saveas(h,'Q2f1800','png');

end

