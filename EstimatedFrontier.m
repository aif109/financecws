function [sigmas,rvecs,weights] = EstimatedFrontier(time, rbar, Cov,N,M)
%ESTIMATEDFRONTIER 
% Inputs:
% time - time length
% rbar - mean values
% Cov - covariance matrix
% N - portofolios numbers
% M - assets' number


warning('off','all');

T = time;
assets = mvnrnd(rbar,Cov,T);

SM = mean(assets,1);
CM = cov(assets);

[sigmas,rvecs, weights] = CalculateFrontier(M, CM, SM, N);

end

