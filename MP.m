function [w,varp] = MP(n, CovM, rvec, rt)
%MP
% solve the markowitz problem with no short selling constraint
% Inputs:
% n - number of assets
% CovM - covariance matrix of asset returns
% rvec - the vector of asset returns for n assets
% rt - target return

% Ouputs:
% w - optimal weights
% varp - min. portofolio variance given the portofolio expected return is r

warning('off','all');

Aeq = [rvec; ones(1,n)];
beq = [rt;1];
[w,varp] = quadprog(CovM,[],[],[],Aeq,beq,zeros(n,1),[]);
varp = 2*varp;
end

