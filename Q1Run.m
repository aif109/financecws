function Q1Run
%Q1RUN 
% by running it we get the solution figures for both part a and b

% solution to part a

face = [100,100,100];
maturity = [120,120,120];
m = 1;
C = [6,10,12];

h=figure(1);
clf

question1(face, maturity, m, C);

xlabel('Yield');
xlim([0.00 0.2]);
ylabel('Price');

hleg = legend('A', 'B', 'C');
set(hleg, 'FontAngle', 'italic', 'TextColor', [.3,.2,.1]);
title('Price-yield curve using annual compounding');

drawnow
saveas(h,'Q1a','png');

% solution to part b

face = [100,100,100];
maturity = [6,12,30];
m = 2;
C = [14,14,14];

h=figure(2);
clf

question1(face, maturity, m, C);

xlabel('Yield');
xlim([0.00 0.2]);
ylabel('Price');
title('Price-Yield curve using semi-annual compounding');

hleg = legend('A', 'B', 'C');
set(hleg, 'FontAngle', 'italic', 'TextColor', [.3,.2,.1]);

drawnow
saveas(h,'Q1b','png');

end

