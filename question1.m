function question1(face, maturity, m, C)
%QUESTION1 
%maturity is in months, so need to divide by 12 to get the number of years.

numberOfPayments = zeros(length(C));

for i=1:length(C)
    numberOfPayments(i) = (maturity(i)/12) * m;
end


% range of yields:
y = 0.00:0.01:0.2;

% prices
for i=1:length(C)
    price = face(i)./(1+y/m).^numberOfPayments(i) + C(i)./y.*(1-1./(1+y/m).^numberOfPayments(i));
    plot(y, price, 'Color', rand(1,length(C)));
    hold all;
end

end

