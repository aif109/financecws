function Markowitz()
%MARKOWITZ 

clc; clear; format short;

rbar = [0.006 0.01 0.014 0.018 0.022];
sigma = [0.085 0.08 0.095 0.09 0.1];
%target = 0.095?

target = mean(rbar); % is this good?

M = length(sigma);

corr = [1 0.3 0.3 0.3 0.3;...
        0.3 1 0.3 0.3 0.3;...
        0.3 0.3 1 0.3 0.3;...
        0.3 0.3 0.3 1 0.3;...
        0.3 0.3 0.3 0.3 1];
    
for i=1:M
    for j=1:M
        Cov(i,j)=sigma(i)*corr(i,j)*sigma(j);
    end
end

%determine the min. variance portofolio return given the target return
[w, varp] = MP(M,Cov,rbar,target);

%determine the portofolio with lowest variance (no constraint on target
%return)
[wmin, varp_min, rmin]=MinVar(M,Cov,rbar);

%determine the portofolio with the highest expected return (no constraint
%on target return)
[wmax, rmax] = MaxExpectedReturn(rbar);

%determine the efficient frontier given a range of target returns
N=10;
for i=1:N
    %efficient frontier
    target = rmin+i*(rmax-rmin)/10;
    [w, varp] = MP(M,Cov,rbar,target);
    rvec(i)=rbar*w;
    sigma_vec(i)=sqrt(varp);
    %inefficient frontier
%     target=rmin-i*(rmax-rmin)/10;
%     [w, varp] = MP(M,Cov,rbar,target);
%     rvec1(i)=rbar*w;
%     sigma_vec1(i)=sqrt(varp);
end



%starting at min. variance portofolio determine the efficient frontier
%given a range of target returns
% step=0.0005;
% rlength=0.06;
% N=round(rlength/step)+1;
% for i=1:N
%     %efficient frontier
%     target=rmin+(i-1)*step;
%     [w,varp]=MP(M,Cov,rbar,target);
%     rvec(i)=rbar*w;
%     sigma_vec(i)=sqrt(varp);
%     
%     %inefficient frontier
%     target=rmin-(i-1)*step;
%     [w,varp]=MP(M,Cov,rbar,target);
%     rvec1(i)=rbar*w;
%     sigma_vec1(i)=sqrt(varp);
% end

figure(1)
clf

plot(sigma_vec, rvec, 'b');%, sigma_vec1, rvec1, 'r');
axis([sigma_vec(1) sigma_vec(N) rmin rmax]);
legend('Efficient Frontier');%, 'Inefficient Frontier');
legend('Location', 'Best');
xlabel('\sigma');
ylabel('r: Expected Portofolio Rate of Return');
title('Efficient Frontier');

end

