function [w, varp, r] = MinVar(n, CovM, rvec)
%MINVAR 
% find the minimum variance point of a portofolio with no short selling
% constraint
% Inputs: 
% n - number of assets
% CovM - cov matrix of assets returns
% rvec - vector of asset returns for n assets
% Outputs:
% w - optimal weights
% varp - minimum portofolio variance
% r - expected return of the minimum variance portofolio
% w - optimal weights

Aeq = ones(1,n);
beq = 1;
[w,varp] = quadprog(CovM,[],[],[],Aeq,beq,zeros(n,1),[]);
varp = 2*varp;
r = rvec*w;
end

