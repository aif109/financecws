function [avgSE,avgSA,avgRE,avgRA] = ExperimentalRun(time,rbar,Cov,N,M,runs)
%EXPERIMENTALRUN 
% Runs exeperiments 10000 times
% Inputs:
% time - time series length
% rbar - mean values
% Cov - cov matrix
% N - portofoliosNumber
% M - assets number
% runs - running times

sumSE = zeros(runs,N);
sumSA = zeros(runs,N);
sumRE = zeros(runs,N);
sumRA = zeros(runs,N);

for j=1:runs
    j
    [sigmas, rvecs, weights] = EstimatedFrontier(time, rbar, Cov, N, M);
    [stds, means] = ActualFrontier(N,M,weights,Cov,rbar);
    sumSE(j,:) = sigmas;
    sumSA(j,:) = stds;
    sumRE(j,:) = rvecs;
    sumRA(j,:) = means;
end

for j=1:N
    sum1 = 0;
    sum2 = 0;
    sum3 = 0;
    sum4 = 0;
    for i=1:runs
        i
        sum1 = sum1 + sumSE(i,j);
        sum2 = sum2 + sumSA(i,j);
        sum3 = sum3 + sumRE(i,j);
        sum4 = sum4 + sumRA(i,j);
    end
    avgSE(j) = sum1/runs;
    avgSA(j) = sum2/runs;
    avgRE(j) = sum3/runs;
    avgRA(j) = sum4/runs;
end

end

