function Q2Run()
%Q2Run
% we run the question 2 and obtain solutions to all of the subparts
% a, b, c, d, e, f

time1 = 24;
time2 = 360;
time3 = 1800;
portofolioNumbers = 10;
runs = 10000;

question2(time1, time2, time3, portofolioNumbers, runs);

end

